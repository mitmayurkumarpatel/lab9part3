package bank;

public class EmployeeFactory {
    private static EmployeeFactory factory;
    
    
    private EmployeeFactory()
    {}
    
    public static EmployeeFactory getInstance()
    {
        if(factory==null)
            factory = new EmployeeFactory();
        return factory;
    }
   
    public Employee getEmployee(EmployeeType type,String name,double wage, double hours)
    {
        switch( type )
        {
            case ASSOCIATE : return new Associate(name, wage, hours);
        }
        
        
        
        
        return null;
    }
}
