
package bank;

/**
 *
 * @author mitpa
 */
public class Employee 
{
    String name;
    double wage;
    double hours;

    public Employee(String name, double wage, double hours) {
        this.name = name;
        this.wage = wage;
        this.hours = hours;
    }

    public Employee(String name) {
        this.name = name;
    }

    public String getEmployeeName() {
        return name;
    }
    
    
    
}
