package bank;

public class Associate extends Employee{
    public Associate(String name, double wages, double hours) {
        super(name, wages, hours);
    }
    
    //getters
    public String getName() {return super.name;}
    public double getWage() {return super.wage;}
    public double getHours() {return super.hours;}
    
    
    
    //method(s)
    public double totalWage(double wage, double hours) {
       return wage*hours;
    }  
}
