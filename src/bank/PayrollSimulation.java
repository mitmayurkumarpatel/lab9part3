
package bank;

/**
 *
 * @author mitpa
 */
public class PayrollSimulation 
{
    public static void main(String[]args)
    {
         Bank bank = new Bank("Axis Bank");
    Employee emp = new Employee("Mit");
    
    System.out.println(emp.getEmployeeName() + "is employee of " +bank.getBankName());
    EmployeeFactory factory = EmployeeFactory.getInstance();
    
    Employee associate1= factory.getEmployee(EmployeeType.ASSOCIATE, "MIT", 20,100);
    Employee manager1= factory.getEmployee(EmployeeType.MANAGER, "KIPLING", 32 ,220);
    }
   
}
